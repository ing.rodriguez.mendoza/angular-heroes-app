import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(
    private authService: AuthService,
    private router: Router)
  {  }
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.authService.verificarAutenticacion()
      .pipe( tap( estadoAutenticado => {
                      if (!estadoAutenticado) {
                        this.router.navigate(['./auth/login'])
                      }
                  } ) );
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]) : Observable<boolean> | boolean  {
      return this.authService.verificarAutenticacion()
      .pipe( tap( estadoAutenticado => {
                      if (!estadoAutenticado) {
                        this.router.navigate(['./auth/login'])
                      }
                  }));      
  }

  ////other way  
  // canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //     //// other way
  //     // if (localStorage.getItem('id')) 
  //     //// other way

  //     // if (this.authService.auth.id) 
  //     // {
  //     //   // canLoad = true;
  //     //   console.log('canActivate', true);
  //     //   console.log(route);
  //     //   console.log(state);
  //     //   return true;
  //     // }
  //     // return false;

  //     // return this.authService.verificarAutenticacion();
  //     return this.authService.verificarAutenticacion()
  //     .pipe( tap( estadoAutenticado => {
  //                     if (!estadoAutenticado) {
  //                       this.router.navigate(['./auth/login'])
  //                     }
  //                 } ) );
  // }


  ////other way
  // canLoad(
  //   route: Route,
  //   segments: UrlSegment[]) : Observable<boolean> | boolean  {

  //     return this.authService.verificarAutenticacion()
  //     .pipe( tap( estadoAutenticado => {
  //                     if (!estadoAutenticado) {
  //                       this.router.navigate(['./auth/login'])
  //                     }
  //                 } ) );
      
  //     // // let canLoad;
  //     // // console.log('canLoad', false);
  //     // // console.log(route);
  //     // // console.log(segments);
  //     // console.log('this.authService.auth',this.authService.auth);

  //     // //// other way
  //     // // if (localStorage.getItem('id')) 
  //     // if (this.authService.auth.id) 
  //     // {
  //     //   // canLoad = true;
  //     //   // console.log('canLoad', canLoad);
  //     //   // console.log(route);
  //     //   // console.log(segments);
  //     //   return true;
  //     // }
  //     // else
  //     // {
  //     //   // canLoad = false;
  //     //   // console.log('canLoad', canLoad);
  //     //   // console.log(route);
  //     //   // console.log(segments);
  //     //   return false;
  //     // }
      
  //     // return canLoad;
  // }

}
