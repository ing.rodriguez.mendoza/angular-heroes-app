import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Auth } from '../interfaces/auth.interface';
import { tap, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl = environment.baseUrl;
  private _auth : Auth | undefined;

  constructor(private http : HttpClient) { }


  get auth() : Auth {
    return { ...this._auth! }
  }

  login(){
    return this.http.get<Auth>(`${this.baseUrl}/usuarios/1`)
    .pipe(
      tap( resp => this._auth = resp ),
      tap( resp => localStorage.setItem( 'token', resp.id) ),
    );
  }

  logout(){
    // this._auth = {}; sino acepta {}
    this._auth = undefined;
    localStorage.removeItem('token');
  }

  verificarAutenticacion() : Observable<boolean> {
    if (!localStorage.getItem('token')) {
      return of(false)
    }
    return this.http.get<Auth>(`${this.baseUrl}/usuarios/1`)
        .pipe(
          map( auth => {
            //console.log('map', auth);
            this._auth = auth;
            return true;
          })
        );
  }

  // //other way  
  // verificarAutenticacion() : Observable<boolean> | boolean {
  //   if (!localStorage.getItem('token')) { 
  //     return false;
  //   }
  //   return this.http.get<Auth>(`${this.baseUrl}/usuarios/1`)
  //       .pipe( 
  //         map( auth => {
  //           console.log('map', auth);
  //           this._auth = auth;
  //           return true;
  //         })
  //       );
  // }

}
