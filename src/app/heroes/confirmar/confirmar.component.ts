import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Heroe } from '../interfaces/heroe.interface';

@Component({
  selector: 'app-confirmar',
  templateUrl: './confirmar.component.html',
  styles: [
  ]
})
export class ConfirmarComponent implements OnInit {

  // superHeroe!: string;

  constructor(
    public dialogRef: MatDialogRef<ConfirmarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Heroe
  ) { }

  ngOnInit(): void {
    console.log('data', this.data);
    // //other way
    // this.superHeroe = this.data.superhero;
  }

  borrar(){
    //el true significa que el usuario si quiere borrar
    this.dialogRef.close(true);
  }

  cerrar() {
    this.dialogRef.close();
  }

}
