import { Component, Input, OnInit } from '@angular/core';
import { Heroe } from '../../interfaces/heroe.interface';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styleUrls: ['./heroe-tarjeta.component.css']
})
export class HeroeTarjetaComponent implements OnInit {

  @Input('heroe') item!: Heroe;
  @Input('existe') existeHeroes: boolean = true;
  // other way
  // @Input('heroe') item: Heroe | undefined;

  constructor() { }

  ngOnInit(): void {
    // const valor = 1;
    //?? operador nullish
    // const resp = valor ?? 'no hay datos';//si el valor es indefinido sale no hay datos
    // console.log('resp', resp);
  }

}
