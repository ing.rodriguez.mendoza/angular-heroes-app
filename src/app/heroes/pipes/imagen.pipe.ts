import { Pipe, PipeTransform } from '@angular/core';
// import { Heroe } from '../interfaces/heroe.interface';

@Pipe({
  name: 'imagen',
  // pure: false //se conviere en un pipe impuro esto quiere decir que cada cambio (ciclo de vida de angular etc) se va recargar el pipe determinado
})
export class ImagenPipe implements PipeTransform {

  // transform(value: unknown): unknown {
  transform(url:string , id: string): string {
    // console.log('url', url);
    // console.log('id', id);
    let rootImg;
    if (!id && !url) {
      rootImg =  'assets/no-image.png'
    } 
    else if(url) {
      rootImg = url;
    }
    else{
      rootImg = `assets/heroes/${id}.jpg`;
    }
    // const pathAssets = 'assets/heroes/';
    // const vl = `${pathAssets}${heroe.id}.jpg`;
    // console.log('resp del pipe', vl) ;
    return rootImg;
  }

}
