import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { debounceTime, distinctUntilChanged, from, fromEvent, last, Observable, of, pluck } from 'rxjs';
import { Heroe } from '../../interfaces/heroe.interface';
import { map, switchMap, tap } from 'rxjs/operators';
import { HeroesService } from '../../services/heroes-services.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
  ]
})
export class SearchComponent implements OnInit {

  termino: string = "";
  heroes: Heroe[] = [];
  heroeSeleccionado!: Heroe | undefined;
  haySugerencias: boolean = false;

  //Operador ! para decirle al typescript que el elemento txtTermino siempre se le va enviar un valor
  @ViewChild('txtTermino') txtTermino!: ElementRef<HTMLInputElement>;

  constructor(private heroesService: HeroesService) { }

  ngOnInit(): void {
  }

  buscando(e: any) {
    // console.log('e', e);
    // from(e).pipe(pluck('target','value')).subscribe(console.log);
    // console.log('this.termino', of(this.termino).pipe( debounceTime(5000)).subscribe(console.log));
    // this.haySugerencias = true;

    if (!this.txtTermino.nativeElement.value) {
      return;
    }

    const obs$ = fromEvent<KeyboardEvent>(this.txtTermino.nativeElement, 'keyup');
    obs$
    .pipe( 
      // debounceTime<KeyboardEvent>(500),
      pluck<KeyboardEvent, any,any>('target','value'),
      // map<any,any>( resp => resp.target.value),
      // distinctUntilChanged(),
      tap(tap=> console.log('tap before', tap)),
      switchMap<string, Observable<Heroe[]>>(
        // resp => this.heroesService.getHeroeById(resp)      
        resp => this.heroesService.getSugerencias(resp.trim())
      ),
      tap(tap=> console.log('tap after', tap))
    )
    .subscribe(
      resp => { 
        this.heroes = resp;
        this.heroes.length>0? this.haySugerencias: !this.haySugerencias;
      }
    )
    // console.log('txtTermino', this.txtTermino.nativeElement.value)
  }

  opcionSeleccionada(e: MatAutocompleteSelectedEvent){
    // console.log('opcionSeleccionada', e);
    if (!e.option.value) {
      this.heroeSeleccionado = undefined;
      return;
    } 
      
      const heroe : Heroe = e.option.value;
      console.log( 'MatAutocompleteSelectedEvent', heroe );
      // this.txtTermino.nativeElement.value = heroe.superhero;
      this.termino = heroe.superhero;
  
      this.heroesService.getHeroeById(heroe.id!).subscribe(resp => this.heroeSeleccionado = resp);      

  }

}
