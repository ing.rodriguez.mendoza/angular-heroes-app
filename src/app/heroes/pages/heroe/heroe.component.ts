import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, ObservableInput } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Heroe } from '../../interfaces/heroe.interface';
import { HeroesService } from '../../services/heroes-services.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: [
    `img {
      width: 100%;
      border-radius: 5px;
    }`
  ]
})
export class HeroeComponent implements OnInit {

  heroe!: Heroe;

  constructor(
    private activatedRoute: ActivatedRoute,
    private heroesService: HeroesService,
    private router: Router) {

  }

  ngOnInit(): void {

    this.activatedRoute.params
    .pipe(
      switchMap<Params,Observable<Heroe>>(
        //other way
        // (param) => this.heroesService.getHeroeById(param['id'])
        ({id}) => this.heroesService.getHeroeById(id)
                                    // .pipe(
                                    //  tap(console.log)
                                    // )
      )
    )
    .subscribe(
      // ({id}) => {
      // //  console.log('id', id)
      // this.heroesService.getHeroeById(id)
      //     .pipe(
      //       tap(console.log)
      //     ).subscribe(console.log)
      // }
      resp => this.heroe = resp
    );
    //  console.log('this.activatedRoute.params',);
  }

  regresar () {
    this.router.navigate(['/heroes/list']);
  }

}
