import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../services/heroes-services.service';
import { Heroe } from '../../interfaces/heroe.interface';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styles: [`    
  `
  ]
})
export class ListComponent implements OnInit {

  heroesList: Heroe[]=[];
  constructor(private heroesService: HeroesService) { }

  ngOnInit(): void {
    console.log('list');
    this.heroesService.getHeroes()
    .pipe( tap( console.log ) )
    .subscribe(
      resp=> {
        this.heroesList= resp;
        console.log('resp', resp);
      } );
  }

}
