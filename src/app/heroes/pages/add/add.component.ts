import { Component, OnInit } from '@angular/core';
import { Heroe, Publisher } from '../../interfaces/heroe.interface';
import { HeroesService } from '../../services/heroes-services.service';

import { ActivatedRoute, Router } from '@angular/router';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { BehaviorSubject, of } from 'rxjs';
import { AjaxError } from "rxjs/ajax";

import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmarComponent } from '../../confirmar/confirmar.component';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styles: [`
    img {
      width: 100%;
      border-radius: 5px;
    }
  `
  ]
})
export class AddComponent implements OnInit {
  selectedValue!: string;
  nameForm: string = 'Nuevo';
  editarForm: boolean = false;
  // heroe!: Heroe;
  heroe: Heroe = {
    superhero:        '',
    publisher:        Publisher.DCComics,
    alter_ego:        '',
    first_appearance: '',
    characters:       '',
    alt_img:          ''
  };

  publishers = [
    {
      id: 'DC Comics',
      desc: 'DC - Comics'
    },
    {
      id: 'Marvel Comics',
      desc: 'Marvel - Comics'
    }
  ];

  constructor(
    private heroesService:HeroesService,
    private activatedRoute:ActivatedRoute,
    private router:Router,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog
    ) 
  { }

  atrapaError = ( err: AjaxError ) => {
    console.warn('error en:', err.message);    
    return of();
  }

  ngOnInit(): void {

    if (!this.heroe.id) {
      this.editarForm = false;
      this.nameForm = 'Nuevo';
    } else {
      this.editarForm = true;
      this.nameForm = 'Editar';
    }

    //si la url no incluye la ruta edit no procede a realizar la búsqueda del parámetro
    if(!this.router.url.includes('edit')) { return }


    this.activatedRoute.params
    .pipe(
      switchMap(
         ({id})=> this.heroesService.getHeroeById(id)
      ),
      catchError(
       this.atrapaError
      )
    )
    .subscribe(
      resp => {
        this.heroe = resp;
        if (!this.heroe.id) {
          this.editarForm = false;
          this.nameForm = 'Nuevo';
        } else {
          this.editarForm = true;
          this.nameForm = 'Editar';
        }
      }
    );
  }

  guardar() {

    if(this.heroe.superhero.trim().length === 0) {return;}

    if (!this.heroe.id) {
      console.log('guardar');
      
      this.heroesService.setHeroe(this.heroe).subscribe(
        ({id}) => {
          console.log('resp guardar', id);
          this.router.navigate(['/heroes/edit/'+ id]);
          this.openSnackBar('Registro creado');
        }
      );      
    } else {
      console.log('editar');
      this.heroesService.updateHeroe(this.heroe).subscribe(
        resp => {
          console.log('resp actualizar', resp);
          this.heroe = resp;
          this.openSnackBar('Registro actualizado');
        }

      )
      // this.heroesService.setHeroe(this.heroe).subscribe(
      //   resp => console.log('resp', resp)
      // );      
    }

    console.log('this.heroe', this.heroe);
  }

  eliminar() {
    const dialog = this.dialog.open(ConfirmarComponent, {
      width: '30%',
      data: {...this.heroe} //le enviamos una copia del objeto this.heroe
    });

    // console.log('dialog', resp);
    dialog.afterClosed()
    .pipe(
      switchMap(
        result => (result) ? this.heroesService.deleteHeroe(this.heroe.id!) : of(false)
        ////other way
        // result => (result) ? this.heroesService.deleteHeroe(this.heroe.id!) : new BehaviorSubject(false)
      ),
      tap( resp => console.log('tap' ,resp))
    )
    .subscribe(
      resp => {
        if (resp) {
              console.log('resp eliminar', resp);
              this.router.navigate(['/heroes']);
        }
      }
    )
  }

  // console.log('e urlNgModelChange: ', e);
  urlNgModelChange( url: string ) {
    this.heroe.alt_img = url;
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', { direction: 'ltr', duration: 2500 });
  }

}
