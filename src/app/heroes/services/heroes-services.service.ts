import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Heroe } from '../interfaces/heroe.interface';


@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  
  private url: string = environment.baseUrl;

  constructor(private http:HttpClient) { }

  //retorna un observable de heroes
  getHeroes(): Observable<Heroe[]>{
    return this.http.get<Heroe[]>(`${this.url}/heroes`);
  }

  getHeroeById(id: string): Observable<Heroe>{
    return this.http.get<Heroe>(`${this.url}/heroes/${id}`);
  }

  getSugerencias(name: string): Observable<Heroe[]>{
    return this.http.get<Heroe[]>(`${this.url}/heroes?q=${name}&_limit=6`);
  }

  setHeroe(heroe: Heroe): Observable<Heroe>{
    return this.http.post<Heroe>(`${this.url}/heroes/`, heroe);
  }

  updateHeroe(heroe: Heroe): Observable<Heroe>{
    return this.http.put<Heroe>(`${this.url}/heroes/${heroe.id}`,heroe);
  }

  deleteHeroe(id: string): Observable<any>{
    return this.http.delete<Heroe>(`${this.url}/heroes/${id}`);
  }
}
